DESC=default

all:
	@git push

salle:
	@sh obj.sh "${MODIF}" "Salle" "${NOM}" "${DESC}"

csalle:
	@sh obj.sh "CREATION" "Salle" "${NOM}" "${DESC}"

dsalle:
	@sh obj.sh "BUGFIX" "Salle" "${NOM}" "${DESC}"

msalle:
	@sh obj.sh "CONTENU" "Salle" "${NOM}" "${DESC}"

object:
	@sh obj.sh "${MODIF}" "Objet" "${NOM}" "${DESC}"

cobject:
	@sh obj.sh "CREATION" "Objet" "${NOM}" "${DESC}"

dobject:
	@sh obj.sh "BUGFIX" "Objet" "${NOM}" "${DESC}"

mobject:
	@sh obj.sh "CONTENU" "Objet" "${NOM}" "${DESC}"

cont:
	@sh obj.sh "${MODIF}" "Boite" "${NOM}" "${DESC}"

ccont:
	@sh obj.sh "CREATION" "Boite" "${NOM}" "${DESC}"

dcont:
	@sh obj.sh "BUGFIX" "Boite" "${NOM}" "${DESC}"

mcont:
	@sh obj.sh "CONTENU" "Boite" "${NOM}" "${DESC}"

port:
	@sh obj.sh "${MODIF}" "PORTAL" "${NOM}" "${DESC}"

cport:
	@sh obj.sh "CREATION" "PORTAL" "${NOM}" "${DESC}"

dport:
	@sh obj.sh "BUGFIX" "PORTAL" "${NOM}" "${DESC}"

mport:
	@sh obj.sh "CONTENU" "PORTAL" "${NOM}" "${DESC}"

glados:
	@sh obj.sh "GLADOS" "MODIF" "${NOM}" "${DESC}"

lolo:
	@git fetch lolo
	@git merge lolo/master

tony:
	@git fetch Tony
	@git merge Tony/master

run:
	@python3 mud.py
