# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .event import Event2, Event3

class MangerEvent(Event2):
    NAME = "manger"

    def perform(self):
        if self.object.has_prop("mangable"):
            return self.inform("manger.failed")
        self.inform("manger")
