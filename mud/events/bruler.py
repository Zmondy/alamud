# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .event import  Event3



class BrulerWithEvent(Event3):
    NAME = "bruler"

    def perform(self):
        if self.object2.has_prop("brulable"):
            return self.inform("bruler.failed")
        if self.object.has_prop("bruleur"):
            return self.inform("bruler.failed")
        self.object.move_to(None)
        self.inform("bruler")