
class Glados:

    def __init__(self):
        self.id = "0"

    def testAction(self,action,actor):
        try:
            exec ('self.cmdNb'+self.id+'(action,actor)')
        except:
            pass


    def testMsg(self,msg):
        if msg == "ah":
            return "Pas de Ah avec moi Denis !"
        return ""

    def cmdNb0(self,action,actor):
        self.id = "1";
        self.sendMsg(actor, "Bonjour et bienvenue au centre d'enrichissement assisté par ordinateur d'Aperture Science.", "https://i1.theportalwiki.net/img/4/43/GLaDOS_00_part1_entry-1_fr.wav")


    def sendMsg(self,actor,msg):
        actor.send_echo("<pre>GlaDOS : %s</pre>" % msg)
        for observer in self.observers(actor):
            observer.send_echo("<pre>GlaDOS : %s</pre>" % msg)

    def sendMsg(self,actor,msg,lien):
        txt = "<a href=\""+lien+"\" target=\"_blank\"> <pre>GlaDOS : %s</pre></a>" % msg
        actor.send_echo(txt)
        for observer in self.observers(actor):
            observer.send_echo(txt)

    def observers(self,actor):
        cont = actor.container()
        if cont:
            for x in cont.contents():
                if x is not self.actor and x.is_player() and x.can_see():
                    yield x